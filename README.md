Simple Ubuntu based docker image with set of tools for debugging on K8s

- By default it starts with infinity sleep so that you can attach your terminal to it for interactive debugging.
- It correctly handles termination signals from the outside.
- It offers `nano` editor, `curl` / `wget` + DNS debug tools.